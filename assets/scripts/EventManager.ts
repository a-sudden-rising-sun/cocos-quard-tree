export class EventManager {
    /**
     * 事件管理数据中心
     *
     * @private
     * @static
     * @type {{ [key: string | number]: any[] }}
     * @memberof EventManager
     */
    private static _handlers: { [key: string | number]: any[] } = {};
  
    /**
     * 监听事件
     *
     * @static
     * @param {string} eventName 事件名称
     * @param {Function} handler 监听函数
     * @param {*} target 监听目标
     * @memberof EventManager
     */
    public static on(eventName: string | number, handler: Function, target: any) {
      let objHandler: {} = { handler, target };
      EventManager._handlers[eventName] = EventManager._handlers[eventName] || [];
      EventManager._handlers[eventName].push(objHandler);
    }
  
    /**
     * 取消监听
     *
     * @static
     * @param {string} eventName 监听事件
     * @param {Function} handler 监听函数
     * @param {*} target 监听目标
     * @return {*}
     * @memberof EventManager
     */
    public static off(
      eventName: string | number,
      handler: Function,
      target: any
    ) {
      let handlerList = EventManager._handlers[eventName];
      if (!handlerList) return;
      for (let i = 0; i < handlerList.length; i++) {
        let oldObj = handlerList[i];
        if (oldObj.handler === handler && (!target || target === oldObj.target)) {
          handlerList.splice(i, 1);
          break;
        }
      }
    }
  
    /**
     * 分发事件
     *
     * @static
     * @param {string} eventName 分发事件名
     * @param {...any} args 分发事件参数
     * @memberof EventManager
     */
    public static dispatchEvent(eventName: string | number, ...args: any) {
      if (!EventManager._handlers[eventName]) {
        console.log(`事件${eventName}当前没有监听者哦`);
        return;
      }
      EventManager._handlers[eventName].forEach((objHandler) => {
        objHandler.handler.apply(objHandler.target, args);
      });
    }
  
    /**
     * 事件初始化
     *
     * @static
     * @memberof EventManager
     */
    public static initEvent() {
      this._handlers = {};
    }
  }