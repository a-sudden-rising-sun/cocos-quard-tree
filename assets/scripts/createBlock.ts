import { _decorator, Color, Component, math, Node, resources, Size, Sprite, SpriteFrame, UITransform, Vec2, Vec3 } from 'cc';
import { customEvent, customEventType } from './event/customEvent';
import { EventManager } from './EventManager';
const { ccclass, property } = _decorator;

@ccclass('crearBlock')
export class createBlock extends Component {
    static number =1
    //2D演示
    public static creatSmall(main:Node){
        let mainContent = main.getComponent(UITransform)
        let width = mainContent.width
        let height = mainContent.height
        if(width<250&&height<250||width<250||height<250){
            return;
        }
        let children = new Node()
        children.name="item-"+this.number
        this.number++
        children.layer=33554432
        children.setParent(main)
        children.setPosition(this.createRandom(width,height))
        children.addComponent(UITransform)
        let transform = children.getComponent(UITransform)
        transform.setContentSize(new Size(50,50))
        children.addComponent(Sprite)
        let sp = children.getComponent(Sprite)
        sp.color=Color.WHITE
        resources.load("default_sprite_splash/spriteFrame",SpriteFrame,(error,sprite)=>{
            sp.spriteFrame=sprite
        })
        sp.sizeMode=0
        customEvent.GetInstance().emit(customEventType.HASBEEN_CREATE,{
            node:children,
            x:children.getPosition().x+640,
            y:children.getPosition().y+360,
            width:transform.width,
            height:transform.height
        },children)
        //EventManager.dispatchEvent("create")
    }
    //随机放置
    public static createRandom(width:number,height:number){
        let randomWith = Math.random()*((width-50)/2)
        let randomHeight = Math.random()*((height-50)/2)
        if(math.random()>0.5){
            randomWith*=-1
        }
        if(math.random()>0.5){
            randomHeight*=-1
        }
        return new Vec3(randomWith,randomHeight,0)
    }
    addToNode(treeNode:Node){

    }
}


