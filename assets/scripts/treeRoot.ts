import { _decorator, Canvas, Color, Component, EventMouse, find, ImageAsset, Input, Node, resources, Size, Sprite, SpriteFrame, Texture2D, UITransform, Vec3, view } from 'cc';
import { customEvent, customEventType } from './event/customEvent';
import { myQuadTree } from './quadTreePlus/myQuadTree';
import { EventManager } from './EventManager';
const { ccclass, property } = _decorator;

@ccclass('treeRoot')
export class treeRoot extends Component {

    private followBlock:Node=null
    private halfWidth
    private halfHeight
    private transform:UITransform
    public rootNode:myQuadTree
    public posMap=new Map()
    start() {
        this.node.on(Input.EventType.MOUSE_DOWN,this.whenClick,this)
        this.node.on(Input.EventType.MOUSE_MOVE,this.mouseMove,this)
        let size = view.getResolutionPolicy().canvasSize
        this.halfHeight=size.height/2
        this.halfWidth=size.width/2
        this.rootNode = new myQuadTree({
            x: 0,
            y: 0,
            width: 1280,
            height: 720
        },4);
        customEvent.GetInstance().register(customEventType.HASBEEN_CREATE,this.insertInTo,true,this)
    }
    update(deltaTime: number) {
        this.node.on
    }

    insertInTo(created,newNode){
        let spline = this.rootNode.insert(created)
        this.posMap.set(created,newNode)
    }
    mouseMove(move:EventMouse){
        if(!this.followBlock){
            return;
        }
        
        // console.info(move.movementX)
        this.followBlock.setPosition(new Vec3(move.getLocationX()-this.halfWidth,move.getLocationY()-this.halfHeight,0))
        // console.info(find("Canvas").getComponent(Canvas))
        // console.info(this.followBlock.getPosition())
        let pos = this.followBlock.getPosition()
        let size = this.transform.contentSize
        let checkNode=this.rootNode.retrieve({
                x: pos.x+640,
	            y: pos.y+360,
	            width: size.width,
	            height: size.height
            })
        this.checkOut(checkNode)
    }
    checkOut(candidates){ 
        for(var i=0;i<candidates.length;i=i+1) {
            this.setSpriteColor(this.posMap.get(candidates[i]))
            candidates[i].check = true;
        }
    }
    whenClick(down:EventMouse){
        if(!this.followBlock){
            this.createNode(new Vec3(down.getLocationX(),down.getLocationY(),0))
        }
        else{
            this.followBlock.setPosition(new Vec3(down.getLocationX(),down.getLocationY(),0))
        }
    }
    setSpriteColor(spNode){
        let sp = spNode.getComponent(Sprite)
        resources.load("green/spriteFrame",SpriteFrame,(err,spf)=>{
            sp.spriteFrame=spf
        })
    }

    createNode(pos:Vec3){
        this.followBlock = new Node()
        this.followBlock.layer=33554432
        this.followBlock.setParent(find("Canvas"))
        this.followBlock.setPosition(pos)
        this.followBlock.addComponent(UITransform)
        this.transform = this.followBlock.getComponent(UITransform)
        this.transform.setContentSize(new Size(25,25))
        this.followBlock.addComponent(Sprite)
        let sp = this.followBlock.getComponent(Sprite)
        sp.color=Color.GRAY
        resources.load("default_sprite_splash/spriteFrame",SpriteFrame,(error,sprite)=>{
            sp.spriteFrame=sprite
        })
        sp.sizeMode=0
    }
}




